'use strict';
var gulp = require('gulp'),
		smushit = require('gulp-smushit');

module.exports = function(options){
	var config = {
		task : {
			default : 'imgmin'
		},
		taskWatch : {
			default : "imgmin:watch"
		},
		dev : {
			default: {
				image : options.dev.imagePath,
        		sass : options.dev.sassPath
			}
		},
		prod : {
			default : {
      			image : options.prod.imagePath 
      		}
		}
	};

	gulp.task(config.task.default, imgminTaskDefault);
	//gulp.task(config.taskWatch.default , imgminTaskWatchDefault);


	function imgminTaskDefault()
	{
		return gulp.src(config.dev.default.image + '**/*.*')
		.pipe(smushit())
		.pipe(gulp.dest(config.prod.default.image));
	}
	function imgminTaskWatchDefault()
	{
		//gulp.watch(config.dev.default.sass, [config.task.default]);
	}




};
