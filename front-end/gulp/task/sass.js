'use strict';
var gulp = require('gulp'),
	sass = require('gulp-sass');

module.exports = function(options){
	var config = {
		task : {
			default : "sass"
		},
		taskWatch : {
			default : "sass:watch",
		},
		dev : {
			default : {
				sass : options.dev.sassFile
			}
		},
		prod : {
			default : {
				css : options.prod.css
			}
		}
	};



	gulp.task(config.task.default, sassTaskDefault);
	gulp.task(config.taskWatch.default , sassTaskWatchDefault);


	function sassTaskDefault()
	{
		console.log(config.dev.default.sass)
		gulp.src(config.dev.default.sass)
			.pipe(sass({
				includePaths: require('node-jeet-sass').includePaths
			}).on('error', sass.logError))
			.pipe(gulp.dest(config.prod.default.css));
		console.log(config.prod.default.css)

	}
	function sassTaskWatchDefault()
	{
		gulp.watch(config.dev.default.sass, [config.task.default]);
	}
};
